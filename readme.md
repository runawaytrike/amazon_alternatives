# TODOS: 


* Add contact with Tutanota email
* Add "have a suggestion?" field thing.
* Remove unused styles; merge in mixins, cleanup current mess.
* Minimize/optimize JS.
* Add a favicon.

* Add npm scripts to bundle sass compile/watch command, eleventy build. 

## Potential Analytics Solutions: 
* Self-hosted DO server. 
* A bunch of good ones, but none free. GA is free. 
* https://www.gosquared.com/analytics/ - free if small. 