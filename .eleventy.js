module.exports = function (eleventyConfig) {
	eleventyConfig.addShortcode(
		'alternative',
		function (name, link, description) {
			return `<div class="alternative">
            <p>
            <a class="alternativeLink" href=${link}>${name}</a>
            ${description}</p>
            </div>`;
		}
	);
};
